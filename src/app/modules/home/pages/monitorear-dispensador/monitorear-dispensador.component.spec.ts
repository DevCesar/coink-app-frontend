import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorearDispensadorComponent } from './monitorear-dispensador.component';

describe('MonitorearDispensadorComponent', () => {
  let component: MonitorearDispensadorComponent;
  let fixture: ComponentFixture<MonitorearDispensadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonitorearDispensadorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonitorearDispensadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
