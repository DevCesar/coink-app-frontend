import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiRickyMorty, InfoApi } from 'src/app/shared/interface/api-ricky-morty';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-crear-dispensador',
  templateUrl: './crear-dispensador.component.html',
  styleUrls: ['./crear-dispensador.component.scss']
})
export class CrearDispensadorComponent implements OnInit {

  data: ApiRickyMorty[] = [];
  infoData!: InfoApi;
  form!: FormGroup;

  constructor(private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.getAllCharacterDatatable();
    this.initFiltro();
  }

  initFiltro() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      create_date: [null]
    })
  }

  getAllCharacterDatatable() {
    this.api.getCharacterDatatable(null).subscribe((result: any) => {
      if (result) {
        this.data = result.results;
        this.infoData = result.info;
      }
    })
  }

  filterClear() {
    this.form.reset();
    this.getAllCharacterDatatable();
  }

  filterData() {
    if (this.form.get('name')?.value === '' || this.form.get('name')?.value == null) {
      //se puede personalizar con librerias como sweetalert2
      //o diseñar la propia alerta
      window.alert('El campo no puede estar vacio')
    } else {
      this.api.getCharacterFilter(this.form.get('name')?.value).subscribe((result: any) => {
        if (result) {
          this.data = result.results;
        }
      })
    }
  }

}
