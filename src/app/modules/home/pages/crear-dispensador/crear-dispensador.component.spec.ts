import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearDispensadorComponent } from './crear-dispensador.component';

describe('CrearDispensadorComponent', () => {
  let component: CrearDispensadorComponent;
  let fixture: ComponentFixture<CrearDispensadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearDispensadorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrearDispensadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
