import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CrearDispensadorComponent } from "./pages/crear-dispensador/crear-dispensador.component";

const routes: Routes = [
  {
    path: 'crear-dispensador',
    component: CrearDispensadorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule { }
