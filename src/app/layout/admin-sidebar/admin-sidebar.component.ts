import { Component, OnInit } from '@angular/core';
import { MenuInterface } from 'src/app/shared/interface/menu-interface';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss']
})
export class AdminSidebarComponent implements OnInit {

  menu: MenuInterface[] = [
    {
      id: 1,
      linkText: "Tarjetas visa en dispensadores",
      parentLink: "",
      menu: false,
      active: true,
      submenu: [
        {
          childtext: "Crear dispensador",
          link: "crear-dispensador"
        },
      ]

    },
    {
      id: 2,
      linkText: "Monitorear y editar dispensadores",
      parentLink: "/monitorearDispensadores",
      menu: true,
      active: false,
    },
    {
      id: 2,
      linkText: "Solicitud tarjetas",
      parentLink: "/solicitudTarjetas",
      menu: true,
      active: false,
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
