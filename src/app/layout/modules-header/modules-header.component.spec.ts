import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesHeaderComponent } from './modules-header.component';

describe('ModulesHeaderComponent', () => {
  let component: ModulesHeaderComponent;
  let fixture: ComponentFixture<ModulesHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulesHeaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModulesHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
