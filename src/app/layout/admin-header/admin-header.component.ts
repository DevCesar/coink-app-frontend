import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  toggled: any = 1;
  dynamicName:string = "BACKOFFICE"

  constructor() { }

  ngOnInit(): void {
  }

  toggleMenu() {
    const element = document.getElementById("sidebar");
    if (this.toggled) {
      if (element != undefined) {
        element.style.width = "0";
        this.toggled = 0;
      }
    } else {
      if (element != undefined) {
        element.style.width = "300px";
        this.toggled = 1;
      }
    }
  }
}
