import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './modules/home/home.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { ProfileComponent } from './user/profile/profile.component';
import { ModulesLayoutComponent } from './layout/modules-layout/modules-layout.component';
import { ModulesHeaderComponent } from './layout/modules-header/modules-header.component';
import { UserHeaderComponent } from './layout/user-header/user-header.component';
import { UserLayoutComponent } from './layout/user-layout/user-layout.component';
import { AdminHeaderComponent } from './layout/admin-header/admin-header.component';
import { AdminSidebarComponent } from './layout/admin-sidebar/admin-sidebar.component';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { CrearDispensadorComponent } from './modules/home/pages/crear-dispensador/crear-dispensador.component';
import { MonitorearDispensadorComponent } from './modules/home/pages/monitorear-dispensador/monitorear-dispensador.component';
import { SolicitudTarjetasComponent } from './modules/home/pages/solicitud-tarjetas/solicitud-tarjetas.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    ModulesLayoutComponent,
    ModulesHeaderComponent,
    UserHeaderComponent,
    UserLayoutComponent,
    AdminHeaderComponent,
    AdminSidebarComponent,
    AdminLayoutComponent,
    CrearDispensadorComponent,
    MonitorearDispensadorComponent,
    SolicitudTarjetasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
