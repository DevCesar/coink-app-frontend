import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getCharacterDatatable(filter: any): Observable<any> {
    return this.http.get(environment.getAllCharacter);
  }

  getCharacterFilter(name: string) {
    return this.http.get(environment.getAllCharacter + `/?name=${name}`);
  }
}
