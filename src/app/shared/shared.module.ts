import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, } from '@angular/common/http';

//Import components
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { GSelectComponent } from "./components/g-select/g-select.component";
import { HeaderComponent } from './components/header/header.component';



const COMPONENTS = [
  HeaderComponent,
  SidebarComponent,
  GSelectComponent
];

const MODULES = [
  CommonModule,
  HttpClientModule,
  FormsModule,
  ReactiveFormsModule
];



@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS, ...MODULES],
})

export class SharedModule { };
