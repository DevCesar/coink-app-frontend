import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GSelectComponent } from './g-select.component';

describe('GSelectComponent', () => {
  let component: GSelectComponent;
  let fixture: ComponentFixture<GSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GSelectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
