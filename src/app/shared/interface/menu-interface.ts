export interface MenuInterface {
  id: number,
  linkText: string,
  parentLink: string,
  menu: boolean,
  active: boolean,
  submenu?: subMenuInterface[]
}

export interface subMenuInterface {
  childtext: string,
  link: string
}
