export interface ApiRickyMorty {
  created: string,
  episode: [],
  gender: string,
  id: number,
  image: string,
  location: {},
  name: string,
  origin: {},
  species: string,
  status: string,
  type: string,
  url: string
}

export interface InfoApi {
  count: number,
  next: string,
  pages: number,
  prev: number | undefined
}
